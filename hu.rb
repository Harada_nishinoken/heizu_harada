# coding: shift_jis
require "dxruby"

Window.caption = "歩" # ウィンドウのキャプション設定
Window.width = 800        # ウィンドウの横サイズ設定
Window.height = 750       # ウィンドウの縦サイズ設定

#盤面
board_lineH = Image.new( 2, 36*20, C_WHITE)
board_lineW = Image.new( 36*20, 2, C_WHITE)
board = Image.new( 36*20, 36*20, C_CYAN)

###########################################################
##### --歩Aのクラスを作成する-- #####
class Hu_A < Sprite

  def initialize(x,y)
    super
    @x = x
    @y = y
    @image_hu = Image.load('hu.png')  # data.pngを読み込む
  end

  def update(x,y) #自身にカーソルがあったら動く
    if(x== @x and y==@y) then
      this.delete
    end
  end

  def draw
    Window.draw( @x, @y, @image_hu);
  end
end
##### --歩Bのクラスを作成する-- #####
class Hu_B < Sprite

  def initialize(x,y)
    super
    @x = x
    @y = y
    @image_hu = Image.load('hu.png')  # data.pngを読み込む
  end

  def update(x,y)#自身にカーソルがあったら動く
    if(x== @x and y==@y) then
      @delete = true
    end
  end

  def draw
    Window.drawRot( @x, @y, @image_hu, 180);
  end

end
###########################################################
##### --カーソルのクラスを作成する-- #####
class Cursol_board < Sprite
  
  def initialize(x,y)
    @x = x+2
    @y = y+2
    @deX = 0
    @deY = 0
    @image_cursor = Image.new( 36-2, 36-2, C_YELLOW)
  end

  def update
    @deX += Input.x
    @deY += Input.y
    if(@deX > 3) then
      @x = @x + 36
      @deX = 0
    elsif(@deX < -3) then
      @x = @x - 36
      @deX = 0
    elsif(@deY > 3) then
      @y = @y + 36
      @deY = 0
    elsif(@deY < -3) then
      @y = @y - 36
      @deY = 0
    end
  end

  def draw
    Window.draw( @x, @y, @image_cursor);
  end

end
###########################################################

i = 0;
k = 0;

#歩とカーソルの宣言
board_hu = Array.new(20){ Array.new(20) }
ObjectGroup = []          # オブジェクト配列
$cursol_board = Cursol_board.new( 0, 0)
for i in 0..5 do
  for k in 0..5 do
    #board_hu[i][k+14] = Hu_B.new( 36*14 + origin_X + 36 * k, origin_Y + 36 * i)
    #board_hu[i+14][k] = Hu_A.new( origin_X + 36 * k, 36*14 + origin_Y + 36 * i)
    ObjectGroup.push( Hu_B.new( 36*14 + 36 * k, 36 * i))
    ObjectGroup.push( Hu_A.new( 36 * k, 36*14 + 36 * i))
  end
end



Window.loop do

  # オブジェクト情報更新
  ObjectGroup.each do |obj|
    obj.update( $cursol_board.x, $cursol_board.y)
  end
  $cursol_board.update

  # 衝突判定
  #ObjectGroup.each do |obj|
    #obj.===($cursol_board)
  #end

  #盤面の描画
  Window.draw( 0, 0, board)
  for i in 0..20 do
    Window.draw( i * 36, 0, board_lineH)
    Window.draw( 0, i * 36, board_lineW)
  end
  #カーソルの描画
  $cursol_board.draw
  # オブジェクトを描画
  ObjectGroup.each do |obj|
    obj.draw
  end

end