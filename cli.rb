#!/usr/bin/ruby

require "socket"
Client_Recv_Size = 20*3*20-1;

# 127.0.0.1(localhost)の20000番へ接続
sock = TCPSocket.open("127.0.0.1", 20000)
#sock = TCPSocket.open("192.168.11.5", 20000)

# サーバから返却された文字列を出力
puts sock.gets(Client_Recv_Size)
puts "recv : defaultMap"

while(1)
  line = $stdin.gets

  # ソケットに入力文字列を渡す
  sock.puts line#"client send process"
  sock.flush
  puts "send end"

  # サーバから返却された文字列を出力
  puts sock.gets(Client_Recv_Size)
  puts "recvA : end"

  # サーバから返却された文字列を出力
  puts sock.gets(Client_Recv_Size)
  puts "recvB : end"


end

# 送信が終わったらソケットを閉じる
sock.close
