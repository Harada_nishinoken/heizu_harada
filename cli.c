#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
#define Recv_Size 20*3*20
#define Send_Size 3*3*36+3

int main(){

    int sd;  //ソケット作成用の変数
    struct sockaddr_in addr;  //サーバ接続用の変数
    char *recv_Map;  //受信データ格納用の変数
    recv_Map = (char *)malloc(sizeof(recv_Map)*Recv_Size);
    char *send_Msg; //送信データ格納用の変数
    send_Msg = (char *)malloc(sizeof(send_Msg)*(Send_Size+2));
    char *p;

    // IPv4 TCP のソケットを作成する
    if((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket");
        return -1;
    }
 
    // 送信先アドレスとポート番号を設定する
    addr.sin_family = AF_INET;
    addr.sin_port = htons(20001);
    addr.sin_addr.s_addr = inet_addr("127.0.0.1");
    //addr.sin_addr.s_addr = inet_addr("192.168.11.5"); 

    // サーバ接続（TCP の場合は、接続を確立する必要がある）
    connect(sd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));

    // パケットを TCP で受信
    if(recv(sd, recv_Map, Recv_Size, 0)<0){
        perror("recv");
        return -1;
    }
    printf("recv.DefaultMap\n%s\n",recv_Map);

    /***  !!!!!!!!!!!!!!!!!!!注意!!!!!!!!!!!!!!!!!!!  ***/
    /***  ポート番号が20000が先攻、20001が後攻とする      ***/
    /***  次のwhile分の中を20000なら"送信"、[受信]、[受信]***/
    /***                 20001なら[受信]、"送信"、[受信] ***/
    /***  の順番に書き換える.                           ***/

    while(1){
      // パケットを TCP で受信
      if(recv(sd, recv_Map, Recv_Size+1, 0)<0){
          perror("recv");
          return -1;
      }
      //改行を取り除いている
      p = strchr(recv_Map, '\n');  
      if(p != NULL) {  
        *p = '\0';  
      }
      printf("recv.Map\n%s\n",recv_Map);

      // 送信データの入力
      strcpy( send_Msg, "999" );
      //scanf("%s",send_Msg);
      strcat( send_Msg, "\n" );

      // パケットを TCP で送信
      if(send(sd, send_Msg, Send_Size+2, 0) < 0) {
          perror("send");
          return -1;
      }
      printf("send end\n");

      // パケットを TCP で受信
      if(recv(sd, recv_Map, Recv_Size+1, 0)<0){
          perror("recv");
          return -1;
      }
      //改行を取り除いている
      p = strchr(recv_Map, '\n');  
      if(p != NULL) {  
        *p = '\0';  
      }
      printf("recv.Map\n%s\n",recv_Map);

      // 無理やりつなぎなおし
      close(sd);
      printf("connect close\n");
      // IPv4 TCP のソケットを作成する
      if((sd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket");
        return -1;
      }
      // 送信先アドレスとポート番号を設定する
      addr.sin_family = AF_INET;
      addr.sin_port = htons(20001);
      addr.sin_addr.s_addr = inet_addr("127.0.0.1");
      connect(sd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in));
      printf("connect reset\n");

    }

    // ソケットを閉じる
    close(sd);
 
    return 0;
}
